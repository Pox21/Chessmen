package Figures;
// класс "Пешка"
public class Pawn extends Figure {
    public Pawn(Color color, int row, int column) {
        super(color, row, column);
    }

    // сеттеров отдельно для строки и столбца не будет - можно только менять координаты вместе
    // "сеттер" для координат
    // public, потому что сеттер
    // не static, потому что для конкретной фигуры
    public void setCoordinates(int row, int column) {
        checkCoordinates(row, column);
        // TODO проверка возможности хода
        // нужен метод, проверяющий свободна ли клетка, а для этого нужна доска)
        if (this.getRow() + 1 != row || this.getColumn() != column) {
            System.out.printf(
                "Пешка может ходить только на 1 клетку вперед и не может ходить по диагонали%n" +
                "Пешка находится на линии: %s в колонке: %s.%n", this.getRow(), this.getColumn()
            );
        } else {
            this.row = row;
            this.column = column;
        }
    }

}
