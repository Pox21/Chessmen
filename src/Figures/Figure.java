package Figures;

public class Figure {
    public enum Color {
        WHITE,
        BLACK,
    }

    protected Color color;
    protected int row;
    protected int column;

    public Figure(Color color, int row, int column) {
        this.color = color;

        checkCoordinates(row, column);
        this.row = row;
        this.column = column;
    }

    // геттер для номера строки
    // public - потому что геттер
    // не static - потому что для конкретной фигуры
    public int getRow() {
        return row;
    }

    // геттер для номера столбца
    public int getColumn() {
        return column;
    }

    public Color getColor() {
        return color;
    }

    // проверка корректности координат
    // private - потому что "служебный"
    // static - не связан с конкретной фигурой, предварительная проверка
    protected static void checkCoordinates(int row, int column) {
        if (row < 1 || row > 8) {
            throw new IllegalArgumentException("Некорректный номер строки: " + row);
            // ошибка - неправильный аргумент
            // выбрасываем её самостоятельно - теперь наш собственный код может сообщать об ошибке
            // throw (объект класса Исключение);
            // throw new КлассИсключения();
            // КлассИсключения() - конструктор по умолчанию, "просто ошибка"
            // КлассИсключения(String сообщениеОбОшибке) - ошибка "с подробностями",
            //  их видно через getMessage()
            // условие-стражник для метода, в котором нельзя просто написать return
        }

        if (column < 1 || column > 8) {
            System.out.println("Некорректный номер столбца: " + column);
            throw new IllegalArgumentException();
        }
    }
}
