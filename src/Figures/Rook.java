package Figures;

public class Rook extends Figure {

    public Rook(Color color, int row, int column) {
        super(color, row, column);
    }

    public void setCoordinates(int row, int column) {
        checkCoordinates(row, column);
        // TODO проверка возможности хода
        // нужен метод, проверяющий свободна ли клетка, а для этого нужна доска)
        if (this.getRow() != row && this.getColumn() != column) {
            System.out.printf(
                "Ладья может ходить только по прямым линиям: горизонталям и вертикалям%n" +
                "Ладья находится на линии: %s в колонке: %s.%n", this.getRow(), this.getColumn()
            );
        } else {
            this.row = row;
            this.column = column;
        }
    }
}
