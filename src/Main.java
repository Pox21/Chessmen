import Figures.Figure;
import Figures.Pawn;
import Figures.Rook;

public class Main {
    public static void main(String[] args) {
        Pawn pawn = new Pawn(Figure.Color.WHITE, 3, 3);
        Rook rook = new Rook(Figure.Color.WHITE, 2, 4);
        System.out.println(pawn.getRow());
        System.out.println(rook.getRow() + "|" + rook.getColumn());
        try {
            pawn.setCoordinates(4, 3);
            rook.setCoordinates(2, 2);
        } catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
        }

        System.out.println(pawn.getRow());
        System.out.println(rook.getRow() + "|" + rook.getColumn());
    }
}